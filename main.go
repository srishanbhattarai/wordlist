package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/fatih/color"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli"
	survey "gopkg.in/AlecAivazis/survey.v1"
)

func main() {
	if err := initCli().Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func initCli() *cli.App {
	app := cli.NewApp()
	app.Version = "0.1.0"
	app.Name = "wordlist"
	app.Usage = "Expand your vocabulary through the terminal"
	app.Action = func(c *cli.Context) error {
		cli.ShowAppHelp(c)

		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:  "where",
			Usage: "Path to the stored wordlist CSV",
			Action: func(c *cli.Context) error {
				fmt.Println(storePath())

				return nil
			},
		},
		{
			Name:  "add",
			Usage: "Add a word to the wordlist",
			Action: func(c *cli.Context) error {
				var qtns = []*survey.Question{
					{
						Name:     "word",
						Prompt:   &survey.Input{Message: "Word"},
						Validate: survey.Required,
					},
					{
						Name:     "meaning",
						Prompt:   &survey.Input{Message: "Meaning"},
						Validate: survey.Required,
					},
					{
						Name:   "example",
						Prompt: &survey.Input{Message: "Example"},
					},
				}

				answers := struct {
					Word    string
					Meaning string
					Example string
				}{}

				err := survey.Ask(qtns, &answers)
				if err != nil {
					die("Could not ask questions: %v", err)
				}

				return addWord(answers.Word, answers.Meaning, answers.Example)
			},
		},
		{
			Name:  "show",
			Usage: "Display the word list",
			Action: func(_ *cli.Context) error {
				return showWords()
			},
		},
	}

	return app
}

func addWord(word, meaning, example string) error {
	w, err := getStore(storePath())
	if err != nil {
		die("Could not open the database file: %v\n", err)
	}

	f := csv.NewWriter(w)

	err = f.Write([]string{word, meaning, example})
	if err != nil {
		die("Could not write to the encoder: %v\n", err)
	}

	f.Flush()
	if err := f.Error(); err != nil {
		die("Could not flush to the database: %v\n", err)
	}

	fmt.Printf("Added word: %s to the database\n", color.CyanString(word))

	return nil

}

func showWords() error {
	f, err := getStore(storePath())
	if err != nil {
		die("Could not open the database file: %v\n", err)
	}

	r := csv.NewReader(f)
	wordlist, err := r.ReadAll()
	if err != nil {
		die("Could not read records: %v\n", err)
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetRowLine(true)
	table.SetHeader([]string{"ID", "Word", "Meaning", "Example"})

	// Append an ID to each row.
	count := 1
	wordlistWithIds := [][]string{}
	for _, v := range wordlist {
		withID := append([]string{strconv.Itoa(count)}, v...)
		wordlistWithIds = append(wordlistWithIds, withID)

		count++
	}

	table.AppendBulk(wordlistWithIds)
	table.Render()

	return nil

}

func storePath() string {
	home, _ := homedir.Dir()

	return home + "/words.csv"
}

func getStore(path string) (*os.File, error) {
	if _, err := os.Stat(path); !os.IsNotExist(err) {
		return os.OpenFile(path, os.O_APPEND|os.O_RDWR, 0644)
	}

	return os.Create(path)
}

func die(message string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, message, args)

	os.Exit(1)
}
